import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TaskListingComponent} from "./task-listing/task-listing.component";
import {TaskViewComponent} from './task-view/task-view.component';

const routes: Routes = [
  {
    path: 'tasks',
    component: TaskListingComponent
  },
  {
    path: 'tasks/:id',
    component: TaskViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
