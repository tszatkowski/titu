import {Component, Input, OnInit, PipeTransform} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Apollo, gql} from 'apollo-angular';
import {Task} from '../task-listing/task-listing.component';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.css']
})
export class TaskViewComponent implements OnInit  {

  @Input()
  task?: Task;
  @Input()
  loading = true;
  @Input()
  errorMessage?: string;

  constructor(private route: ActivatedRoute, private apollo: Apollo) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.apollo.query<{ getTask: Task }>({
        query: gql`query Task($id: Int!) { getTask(id: $id) { name, description, stub } }`,
        variables: {
          id: params['id']
        }
      }).subscribe(result => {
        this.loading = false;
        this.task = result.data.getTask;
      }, error => {
        this.loading = false;
        this.errorMessage = error.toString()
      })
    });
  }

  submit() {

  }

}
