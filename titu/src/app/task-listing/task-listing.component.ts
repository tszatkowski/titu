import {Component, Input, OnInit} from '@angular/core';
import {Apollo, gql} from "apollo-angular";

export interface Task {
  id: number,
  name: string,
  description: string,
  stub: string
}

@Component({
  selector: 'app-task-listing',
  templateUrl: './task-listing.component.html',
  styleUrls: ['./task-listing.component.css']
})
export class TaskListingComponent implements OnInit {

  @Input()
  tasks: Task[] = []

  constructor(private apollo: Apollo) { }

  ngOnInit(): void {
    this.apollo.query<{tasks: Task[]}>({
      query: gql`query { tasks { id, name } }`
    }).subscribe(result => {
      this.tasks = result.data.tasks;
    })
  }

}
