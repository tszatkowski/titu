package titu;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@RequiredArgsConstructor
public class Query implements GraphQLQueryResolver {
  @NonNull
  private SessionFactory sessionFactory;

  public List<Task> getTasks() {
      var session = sessionFactory.openSession();
      var query = session.createQuery("FROM Task t");
      return query.getResultList();
  }

  public Task getTask(int id) {
    var session = sessionFactory.openSession();
    return session.get(Task.class, id);
  }
}
