package titu;

import graphql.kickstart.tools.GraphQLSubscriptionResolver;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.stereotype.Repository;

@Repository
public class Subscription implements GraphQLSubscriptionResolver {
  Publisher<Sock> getSocks() {
    return (Subscriber<? super Sock> s) -> {
      new Thread(() -> {
        try {
          Thread.sleep(3000);
          var e = new Sock();
          e.setName("new");
          e.setSize(4);
          s.onNext(e);
          Thread.sleep(3000);
          var y = new Sock();
          y.setName("new");
          y.setSize(4);
          s.onNext(y);
          s.onComplete();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

      }).run();
    };
  }
}
