package titu;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Sock {
    @Id
    private int id;
    private String name;
    private int size;
}
