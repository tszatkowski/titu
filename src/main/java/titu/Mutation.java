package titu;

import graphql.kickstart.tools.GraphQLMutationResolver;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class Mutation implements GraphQLMutationResolver {
    @Autowired
    SessionFactory sessionFactory;

    @Transactional
    Sock getSocks(String name, int size) {
        var sock = new Sock();
        sock.setName(name);
        sock.setSize(size);
        var session = sessionFactory.openSession();
        var tx = session.beginTransaction();
        session.save(sock);
        tx.commit();
        session.close();
        return sock;
    }
}
