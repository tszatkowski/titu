package titu;

import graphql.kickstart.tools.SchemaParser;
import lombok.SneakyThrows;
import org.postgresql.Driver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import java.util.Properties;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }

    @Bean
    SchemaParser getParser(Query q, Mutation m, Subscription s) {
        return SchemaParser.newParser()
                .file("def.graphql")
                .resolvers(q, m, s)
                .build();
    }

    @Bean
    @SneakyThrows
    LocalSessionFactoryBean initDb() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(
                "hibernate.hbm2ddl.auto", "update");
        hibernateProperties.setProperty(
                "hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        var dataSource = new SimpleDriverDataSource(new Driver(), "jdbc:postgresql://localhost/titu?user=postgres&password=postgres");
        var sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setHibernateProperties(hibernateProperties);
        sessionFactory.setPackagesToScan("titu");
        return sessionFactory;
    }

}
